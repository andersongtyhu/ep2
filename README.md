

# EP2 - OO 2019.2 (UnB - Gama)

Turmas Renato 
Data de entrega: 12/11/2019
EStudante : Anderson silva gomes - 180013106
## Descrição

Para este Ep o principal objetivo será a implementação do jogo Snake("jogo da cobrinha"),é um jogo mundialmente conhecido com atualmente diversas versões,para diversas plataformas,celulares,consoles,computadores dentre outros.Sua versão inicial foi nos anos 90 em celulares da Nookia.


O jogador controla uma longa e fina criatura que se arrasta pela tela, coletando comida (ou algum outro item), não podendo colidir com seu próprio corpo ou as "paredes" que cercam a área de jogo. Cada vez que a serpente come um pedaço de comida, seu rabo cresce, aumentando a dificuldade do jogo. O usuário controla a direção da cabeça da serpente (para cima, para baixo, esquerda e direita) e seu corpo segue.

Para este EP o jogo deve conter barreiras em seu cenário, além das especificações abaixo.

## Game Loop

O usuário terá interatividade com a máquina,controlando as respectivas direcoes da cobrinha,se a cobrinha colidir com ela mesma,aparecerá na tela uma mensagem de Game Over,se a cobrinha encostar na borda da tela tambem aparecera esta mensagem,o objetivo é coletar o maximo de frutas existentes no jogo
## Tipos de Snakes

## Colisões

As Snakes que colidir com barreiras no interior do jogo tanto nas barreiras, quanto elas mesmas, a Snake deve morrer.

## Frutas

As frutas são elementos que aparecem aleatoriamente e são os objetivos das Snakes. As frutas devem desaparecer em um tempo especifico e não devem aparecer mais de duas frutas por vez. 


## Pontos

Os pontos são calculados de acordo com as frutas coletadas.


## Orientações

Para a execução do app terá de ter instalado em sua máquina o eclipse, podendo ser facilmente instalado se maquina for linux pela loja de fabrica, gratuitamente.

se sua maquina for windows basta intalar o arquivo do Google


